*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://drone.dotya.ml/wanderer/docker-archlinux-cdev/
${BROWSER}  chrome
${RANDELEM}  //section[@class='repo-item build-yes active-yes']

*** Test Cases ***
Headless Chrome - Open Browser
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Set Selenium Speed  0.2
    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}

    Sleep  1
    Capture Page Screenshot
    Maximize Browser Window
    Capture Page Screenshot
    Sleep  1
    Click Element  ${RANDELEM}
    Sleep  1
    Capture Page Screenshot

Post-conditions
    Log Location
    Sleep  1
    Close Browser
